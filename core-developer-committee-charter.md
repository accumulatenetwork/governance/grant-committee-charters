# Core Developer Committee Interim Charter 
 
## Objective 
The purpose of this document is to establish the processes for the Core Developer Committee to manage development for the core code and infrastructure for Accumulate.
 
## Core Developer definition 
A Core developer is an engineer working on Accumulate’s core functionality and infrastructure.  Such work can include contributions to the protocol itself, the reference wallet, support for explorers, support for developers, management of core services.
 
## Initial Committee Members 
The initial interim committee will be made up of five members. Confirmed initial committee members: 

- Ben Stolman
- Paul Snow
- Ethan Reesor
- Dennis Bunfield
- Stuart Johnson
- Michael LeSane
 
# Committee Chair 
Responsible for leading meetings, coordinating with other committees to access priorities, market needs, and developer needs.  Coordinates the evaluation of proposals for improvements from the community.
 
# Accumulate Improvement Proposals (AIPs) 
Factom established a process for collecting and executing ideas through the [Factom Improvement Proposals (FIPs)](https://github.com/factom-protocol/FIP/blob/master/FIPS/x.md).  Accumulate will build upon the processes described for FIPs.  As we meet and begin work past the Core Developer Committee Interim Charter, AIPs will be refined for greater on-chain coordination.
 
# Committee Organization 
Communication between committee members can be done both synchronously and asynchronously. A Discord channel in the Accumulate Discord will be set up for committee members to discuss work in progress, protocol issues, and AIPs. This channel can be made publicly viewable, but will only be writeable by committee members. Exceptions can be made for Accumulate stakeholders whose opinion is needed. Meetings can be scheduled ad hoc if synchronous discussion is needed. 
 
In addition, the Core Development committee should have a routinely scheduled Review and Retrospective meeting (starting on a monthly basis and adjusted as needed). This meeting should be used to review the state of all products that have been approved grants and overall performance of the grantee development groups, as well as to review the performance of the committee itself. 
 
# Decisions 
The Core Developer Committee will work towards coordinating efforts in the ecosystem and the efforts of the core developers.  Obviously, not all work on Accumulate infrastructure will have any exposure to the Core Developer Committee. Accumulate will always strive to accelerate the ecosystem, at least to the extent that we can coordinate and promote significant efforts using Accumulate.

FIPs will be evaluated, and the Core Developer Committee act to facilitate the AIP process as described in format about AIPs patterned after FIPs
