#

# Governance Committee Interim Charter

## Objective

This document establishes an interim Grant Approval procedure to be used by the Governance Committee for the processing of grant proposals using a streamlined approach between now and the full launch of the Accumulate mainnet. The interim procedure will act as a steppingstone toward the official, community-driven Grant Approval procedure and will allow key Accumulate ecosystem initiatives to be started in conjunction with the full launch of the protocol.

## Areas of Responsibility

The Governance Committee has primary jurisdiction over grants that fall in the following areas:

- **Protocol Oversight and Operations –** The protocol may wish to engage outside parties to provide oversight of aspects of the protocol or to provide operational support. The Governance committee will be charged with maintaining relationships with and overseeing the work of any organizations working for the protocol.
- **Staking** – Staking is key to the Accumulate security model and the staking model is expected to evolve over time. Staking must balance the protocols need for security while ensuring the model continues to attract organizations to serve as validators. The Governance Committee will be responsible for updating the staking model as required to meet the twin goals.
- **Exchanges and Liquidity** – Accumulate exists in and depends on a tokenomic ecosystem to function. Operating the nodes that support the protocol generates a commodity, the Acme token, which must have a value and be liquid for the Accumulate ecosystem to work. The Governance Committee is responsible for maintaining relationships with outside organization like exchanges and market makers to insure the health of the tokenomic ecosystem.

## Initial Committee Members

The initial interim committee will be made up of five members. Confirmed initial committee members:

- Jay Smith (DeFi Devs) - Chair
- Niels Klomp (Sphereon)
- Sundar Padmanabhan (DeFi Devs)
- Jason Gregoire (Kompendium)
- Quentin Vidal (DeFi Devs)

## Committee Chair

The committee chair is responsible for the following:

- Defining and maintaining consensus for a strategy for each major area of responsibility.
- Defining and executing a communications strategy between the committee members, other committee heads, and key protocol stakeholders.
- Defining and documenting the processes by which the committees and the foundation will interact as well as maintaining community consensus for those processes.
- Tracking and documenting proposals submitted and ensuring timely decision-making.
- Scheduling and facilitating committee meetings as needed.

## Grant Types

This committee will be responsible for reviewing and approving or denying all grant types for Governance products that will be paid for with ACME in the Accumulate grant pool.

- Accumulate Open Calls / Requests for Proposal (RFP)
- Grant Applications from the community
- Fast-track Grant Requests

Please review Accumulate Grant System for more information

## Grant Submission

Grant proposals are to be submitted using a Google Form (or similar service) that will be hosted on the Accumulate website. RFPs can be broadcast through the Accumulate social media channels and website, but should link to the same Google Form.

#### The proposal form should include:

- Applicants contact information (including email and Discord username)
- Summary of the product
- Value product brings the protocol
- Requested ACME amount
- Description of use of funds
- Multimedia material (presentations, videos, etc.)
- GitHub, Gitlab, or Bitbucket account
- Portfolio or links to other projects

If further information is needed before a decision can be made, the committee can request a Demo from the applicant.

## Grant Payment

Budget: 25% of grant pool – At launch the pool will be 60M giving a workstream budget of 15M million ACME (Please review the [Budgets](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#budgets) section of Accumulate Grant System for more information)

Grants may be approved prior to the Accumulate Hard Fork ACME in which case the grants will be for tokens to be issued once the fork occurs. Due to the uncertainty around value of the tokens at launch and the volatility of token values, grants will be limited in size and broken into smaller milestones which will be, upon approval, issued at the start of each milestone thereby minimizing the currency risk taken on by an applicant.

## Committee Organization

Communication between committee members can be done both synchronously and asynchronously. A Discord channel in the Accumulate Discord will be set up for committee members to discuss proposals that have been received. This channel can be made publicly viewable but will only be writeable by committee members. Exceptions can be made for Accumulate stakeholders whose opinion is needed. Meetings can be scheduled ad hoc if synchronous discussion is needed.

In addition, the Governance committee should have a routinely scheduled Review and Retrospective meeting (starting on a monthly basis and adjusted as needed). This meeting should be used to review the state of all products that have been approved grants and overall performance of the grantee development groups, as well as to review the performance of the committee itself.

## Decisions

Committee members must come to a decision on a grant within two weeks of a proposal being received. The committee must come to one of the following decisions:

- Approved: The grant proposal has been approved.
- Tentative Approval: The grant proposal may be approved if certain changes are made to the proposal and then resubmitted (e.g. grant amount or scope of project).
- Rejected: The grant proposal will be declined and cannot be resubmitted.
- Decision Postponed: The grant proposal cannot be approved or denied at the current time but should be taken up at a later date within a reasonable timeframe.

If a Grant Proposal is approved, the applicant should be required to submit a git repository link for the product. The applicant should also be required to submit a monthly status report.

## Scoring and decision-making procedure

Please see [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md) for more information on scoring.

You can view the list of submitted grant applications and their decisions [here](https://docs.google.com/spreadsheets/d/1gO3glqcqJDyUcoaosiIkaBJxn07KJIKT4KgX-wAW6Ug/edit?usp=sharing)
