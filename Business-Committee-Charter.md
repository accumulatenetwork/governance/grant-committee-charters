# Business Committee Interim Charter

##

## Objective

The purpose of this document is to establish an interim Grant Approval procedure for the Business Committee to allow grant proposals to be processed using a streamlined approach between now and the full launch of the Accumulate mainnet. The interim procedure will act as a stepping stone toward the official, community-sanctioned Grant Approval procedure.

## Business committee definition

Grant proposals that fall under the purview of the Business Committee include all applications, tools, solutions, and services that provide for-proft business gain to the Accumulate protocol. The Business Committee is comprised of representatives from marketing and business development working in a synergistic relationship. The overall objective of the Business Committee is to promote the products and services of the protocol, acquire new partners, and grow the community. Specific tasks related to marketing and business development are summarized below.

Marketing activities:

- **Marketing Programs:** any coordinated strategy related to branding, social media, public relations, press releases, and distribution of promotional materials.
- **Community Growth:** any initiative that grows the Accumulate community on its various channels (e.g. Twitter, Telegram, YouTube, Discord, Reddit).
- **Exchange Promotions:** any campaign related to an exchange that encourages users of the exchange to learn about ACME and its utility in the protocol.
- **Ecosystem Marketing:** any co-marketing with ecosystem projects that have mutually beneficial growth for Accumulate and ecosystem projects.
- **Marketing Tools:** any program or application that can automate aspects of the promotion of the Accumulate protocol.
- **Incentives:** any incentivization structure that provides small amounts of ACME to users in order to increase the usage of the protocol.

Business development activities:

- **Business Partnerships:** any initiative to integrate Accumulate features with client products that may benefit the client and/or their customers.
- **Market Opportunities:** any research effort aimed at identifying market trends and the value-add of Accumulate.
- **Business Expansion:** any activities that focus on the growth of existing product pipelines to expand their focus and/or reach.
- **Fee-based Services:** any initiative to sell Accumulate products or its developers to build Software-as-a-Service (SaaS) applications.

All applications paid for by an Accumulate grant should be developed under an open-source license.

## Initial Committee Members

The initial interim committee will be made up of five members.

Confirmed initial committee members:

- Jay Singh, Chairperson
- Kyle Michelson, Vice Chairperson
- Kristine Thomassen
- Aktham Dabbas
- TJ Ometoruwa

Decisions are made with a simple majority. In the case of a tie, the Chair will have two votes.

## Committee Chairperson

The committee chairperson is responsible for tracking and documenting all proposals, and ensuring timely decision-making by the committee. The committee chairperson should document all proposals and their current status in a spreadsheet that is accessible by all committee members. The committee chairperson is also responsible for scheduling meetings of the committee as needed.

## Grant Types

This committee will be responsible for reviewing and approving or denying all grant types for ecosystem products that will be paid for with ACME in the Accumulate grant pool.

- Accumulate Open Calls / Requests for Proposal (RFP)
- Grant Applications from the community
- Fast-track Grant Requests

Please review [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#three-types-of-grant-programs) for more information.

## Grant Submission

Grant proposals are to be submitted using a Google Form (or similar service) that will be hosted on the Accumulate website. RFPs can be broadcast through the Accumulate social media channels and website, but should link to the same Google Form.

The proposal form should include:

- Applicant contact information (including email)
- Summary of the business program
- The measurable metrics of the program
- Requested ACME amount
- Description of use of funds
- Multimedia material (presentations, videos, etc)
- Portfolio or links to relevant projects

If further information is needed before a decision can be made, the committee can request a Demo from the applicant.

## Grant Payment

Budget: 25% of grant pool – 15M ACME (Please review the Budget section of [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#budgets) for more information)

Because the ACME allocated towards the Marketing grant budget will not be available until mainnet launch, the only payment agreement option available for the Marketing Committee is an agreement for future tokens. Payment can also be distributed in installments, to ensure progress.

## Committee Organization

Communication between committee members can be done both synchronously and asynchronously. A Discord channel in the Accumulate Discord will be set up for committee members to discuss proposals that have been received. This channel can be made publicly viewable, but will only be writeable by committee members. Exceptions can be made for Accumulate stakeholders whose opinion is needed. Meetings can be scheduled ad hoc if synchronous discussion is needed.

In addition, the Business committee should have a routinely scheduled Review and Retrospective meeting (starting on a monthly basis and adjusted as needed). This meeting should be used to review the state of all products that have been approved grants and overall performance of the grantee development groups, as well to review the performance of the committee itself.

## Decisions

Committee members must come to a decision on a grant within two weeks of a proposal being received. The committee must come to one of the following decisions:

- **Approved** : The grant proposal has been approved.
- **Tentative Approval:** The grant proposal may be approved if certain changes are made to the proposal and then resubmitted (e.g. grant amount or scope of project).
- **Rejected:** The grant proposal will be declined and cannot be resubmitted.
- **Decision Postponed:** The grant proposal cannot be approved or denied at the current time, but should be taken up at a later date within a reasonable timeframe.

If a Grant Proposal is approved, the applicant should be required to submit a git repository link for the product. The applicant should also be required to submit a monthly status report.

## Agreement Procedure

Please see [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md) for more information on scoring.

You can view the list of submitted grant applications and their decisions [here](https://docs.google.com/spreadsheets/d/1L3CyMiaH_VQ8xJzPPCcrDOZRMYF-8q0IT6uNhl9qKBQ/edit?usp=sharing)
