# Overview
The Committee Charters repository holds the charters for the **four committees that govern the Accumulate network**. For more information on the role of the committees, please see [here](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Committees.md#committees).

## Interim Committee Members

| Governance Committee | Core Development Committee | Ecosystem Committee  | Business Committee     |
|----------------------|----------------------------|----------------------|------------------------|
| Jay Smith (chair)    | Ben Stolman (chair)        | Quentin Vidal (chair)| Kyle Michelson (chair) |
| Niels Klomp          | Paul Snow                  | Maarten Boender      | Kristine Thomasson     |
| Sundar Padmanabhan   | Ethan Reesor               | Kyle Michelson       | Aktham Dabbas          |
| Quentin Vidal        | Dennis Bunfield            | Sundar Padmanabhan   | Anton Ilzheev          |
| Jason Gregoire       | Stuart Johnson             | Jason Gregoire       | Ben Stolman            |
|                      | Michael LeSane             |                      |                        |

Please note that the above listed committee members are **interim** members, which will be replaced once an election system for the committees can be set up.
